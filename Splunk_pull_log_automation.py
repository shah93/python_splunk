#Author: Shahnawaz
#About : splunk_automation

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from openpyxl import load_workbook
import time
import os
import glob
import shutil

wd = webdriver.Chrome('path_to_chrome_driver')

base_url = 'SPLUNK_URL'
USERNAME = 'user_name'
PASSWORD = 'password'
wd.get(base_url)

userid = wd.find_element_by_xpath('//*[@id="username"]');

userid.send_keys(USERNAME);

password = wd.find_element_by_xpath('//*[@id="password"]');
password.send_keys(PASSWORD);

sign_in = wd.find_element_by_xpath('//*[@class="splButton-primary btn"]');
sign_in.click();

wd.implicitly_wait(15)

#openpyxl - excel read action (Read sample string)
ex = load_workbook(os.path.abspath(os.path.join(os.path.dirname(__file__),'path_to_excel_to_read')))

sheet = ex['shee_name'];
c = sheet['column_row'].value ## e.g - sheet['B2'].value
wd.implicitly_wait(15)
print(c) #output to console

actions = ActionChains(wd);
sf = wd.find_element_by_xpath("//textarea");
actions.move_to_element(sf).click().send_keys(c).perform();

#expand search caret

cr = wd.find_element_by_xpath('//span[@class="caret"]')
cr.click()

#select_time -- can be dynamic through excel
Telement = WebDriverWait(wd, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "Last 7 days")))
Telement.click()


# Hit on search
sr_btn= wd.find_element_by_xpath('//a[@class="btn"]')
sr_btn.click()

#Hit on Download button
wd.implicitly_wait(20)
dw_btn = wd.find_element_by_xpath('//a[@class="export btn-pill btn-square shared-jobstatus-buttons-exportbutton"]')
dw_btn.click()

#select download format

format = wd.find_element_by_xpath('//div[@class="modal fade shared-jobstatus-buttons-exportresultsdialog in"]/div[@class="modal-body modal-body-scrolling"]/div[@class="form form-horizontal form-justified"]/div[@class="control-group shared-controls-controlgroup control-group-default"]/div/div/a/span[@class="caret"]')
format.click();

#Download file as a Text (selects Raw Events)
raw = WebDriverWait(wd, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, "Raw Events")))
raw.click()

#click on export button
export = wd.find_element_by_xpath('//a[@class="btn btn-primary modal-btn-primary"]')
export.click()
wd.implicitly_wait(15)
time.sleep(30)

#get latest downloaded file
list_of_files = glob.glob('path_to_download_folder\*')
latest_file = max(list_of_files, key=os.path.getctime)
print latest_file

#move log_file to desktop
shutil.move(latest_file, "path_to_desktop")


